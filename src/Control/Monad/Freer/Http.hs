{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedStrings #-}

module Control.Monad.Freer.Http
    ( Http(..)
    , get
    , getJSON
    , post
    , postJSON
    , doRequest
    , request
    , requestJSON
    , HttpException
    , AesonParseError(..)
    , Exc
    , runHttp
    , mockHttp
    , staticHttpMock
    ) where

import Control.Exception

import Control.Monad

import Control.Monad.Freer
import Control.Monad.Freer.Exception

import Data.Aeson

import qualified Data.ByteString as BS
import Data.ByteString.Lazy (ByteString)

import Data.Map (Map)
import qualified Data.Map as M

import Data.Maybe (fromMaybe)

import Network.HTTP.Client (Request, Response, Manager, httpLbs
                           ,HttpException(HttpExceptionRequest), parseRequest, requestHeaders
                           ,method, path, HttpExceptionContent(StatusCodeException)
                           ,RequestBody(..), requestBody)
import Network.HTTP.Client.Internal (Response(..), CookieJar(CJ), ResponseClose(..))
import Network.HTTP.Types.Header (RequestHeaders)
import Network.HTTP.Types.Method (Method, methodGet, methodPost)
import Network.HTTP.Types.Status (Status, status404, statusCode)
import Network.HTTP.Types.Version (http11)

data Http v where
    DoRequest :: Request -> Http (Response ByteString)

doRequest :: Member Http r => Request -> Eff r (Response ByteString)
doRequest req = send (DoRequest req)

request :: (Member Http r, Member (Exc HttpException) r)
        => Method
        -> RequestHeaders
        -> String
        -> Maybe RequestBody
        -> Eff r (Response ByteString)
request meth headers url body = let
    eReq = parseRequest url
  in case eReq of
       Left some -> case fromException some of
                      Just (err :: HttpException) -> throwError err
                      Nothing -> throw some -- evil?
       Right req -> doRequest req { requestHeaders = headers
                                  , method = meth
                                  , requestBody = fromMaybe mempty body
                                  }

newtype AesonParseError = AesonParseError String deriving (Show)

requestJSON :: (FromJSON resp, ToJSON req
               ,Member Http r, Member (Exc HttpException) r, Member (Exc AesonParseError) r
               )
            => Method
            -> RequestHeaders
            -> String
            -> Maybe req
            -> Eff r resp
requestJSON meth headers url mBody = do
  let body = maybe mempty (RequestBodyLBS . encode) mBody
      eReq = parseRequest url
  req <- case eReq of
       Left some -> case fromException some of
                      Just (err :: HttpException) -> throwError err
                      Nothing -> throw some -- evil?
       Right req -> return $ req { requestHeaders = headers
                                 , method = meth
                                 , requestBody = body
                                 }
  response <- doRequest req
  let code = statusCode $ responseStatus response
  unless (200 <= code && code < 300) $ throwError (HttpExceptionRequest req $ StatusCodeException (() <$ response) "")
  let body = responseBody response
  case eitherDecode body of
    Left err -> throwError $ AesonParseError err
    Right val -> return val


get :: (Member Http r, Member (Exc HttpException) r)
    => String -> Eff r (Response ByteString)
get url = request methodGet [] url Nothing

getJSON :: (FromJSON a, Member Http r, Member (Exc HttpException) r, Member (Exc AesonParseError) r)
        => String -> Eff r a
getJSON url = requestJSON methodGet [] url (Nothing @())

post :: (Member Http r, Member (Exc HttpException) r)
     => String -> RequestBody -> Eff r (Response ByteString)
post url = request methodPost [] url . Just

postJSON :: (FromJSON resp, ToJSON req
            ,Member Http r, Member (Exc HttpException) r, Member (Exc AesonParseError) r
            )
         => String -> req -> Eff r resp
postJSON url = requestJSON methodPost [("Content-Type", "application/json")] url . Just


runHttp :: (Member IO r) => Manager -> Eff (Http ': r) w -> Eff r w
runHttp mgr eff = runNat @IO (\(DoRequest req) -> httpLbs req mgr) eff

mockHttp :: Eff (Http ': r) w -> (Request -> Response ByteString) -> Eff r w
mockHttp eff f = handleRelay return (\(DoRequest req) next -> next (f req)) eff

staticHttpMock :: Eff (Http ': r) w -> Map (Method, BS.ByteString) (Status, ByteString) -> Eff r w
staticHttpMock eff endpoints = mockHttp eff findEndpoint
    where findEndpoint req = case M.lookup (method req, path req) endpoints of
                               Nothing -> Response status404 http11 [] "" (CJ []) (ResponseClose $ return ())
                               Just (s, b) -> Response s http11 [] b (CJ []) (ResponseClose $ return ())
